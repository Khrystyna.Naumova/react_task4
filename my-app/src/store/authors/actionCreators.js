import * as actions from './actionTypes';

export const saveAuthorCreator = (payload) => ({
  type: actions.SAVE_AUTHOR,
  payload,
});

export const getAuthorsCreator = (payload) => ({
  type: actions.GET_AUTHORS,
  payload,
});
