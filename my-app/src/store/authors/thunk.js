import { saveAuthorCreator } from './actionCreators';

export const saveNewAuthorDB = (newAuthor, token) => {
  return (dispatch) => {
    fetch('http://localhost:4000/authors/add', {
      method: 'POST',
      body: JSON.stringify(newAuthor),
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
    })
      .then((res) => res.json())
      .then((data) => dispatch(saveAuthorCreator(data.result)));
  };
};

//getAllAuthors
