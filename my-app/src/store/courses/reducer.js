import * as actions from './actionTypes';

const coursesInitialState = []; // default value - empty array. After success getting courses from API - array of courses.

export const coursesReducer = (state = coursesInitialState, action) => {
  switch (action.type) {
    case actions.SAVE_NEW_COURSE:
      return [...state, action.payload];
    case actions.DELETE_COURSE:
      return state.filter((item) => item.id !== action.payload);
    case actions.UPDATE_COURSE:
      return state.map((item) => {
        if (item.id === action.payload.id) {
          return action.payload;
        }
        return item;
      });
    case actions.GET_COURSES:
      return [...state, ...action.payload.result];
    default:
      return state;
  }
};
