import { Link, useParams } from 'react-router-dom';
import pipeDuration from '../../helpers/pipeDuration';

const CourseInfo = (props) => {
  const { courseId } = useParams();

  const courseInfo = props.courses.find((item) => {
    return item.id === courseId;
  });

  const duration = pipeDuration(courseInfo.duration);
  const authNames = courseInfo.authors.join(', ');

  return (
    <div className='courses'>
      <br></br>
      <Link to='/courses'>
        <span style={{ marginLeft: '1.4rem' }}> ˂ Back to courses </span>
      </Link>
      <div className='courseCard'>
        <div className='courseCard__1'>
          <h1>{courseInfo.title}</h1>
          <p>{courseInfo.description}</p>
        </div>
        <div className='courseCard__2'>
          <p>
            <span>ID:</span> {courseInfo.id}
          </p>
          <p>
            <span>Duration:</span> {duration}
          </p>
          <p>
            <span>Created:</span> {courseInfo.creationDate}
          </p>
          <p>
            <span>Authors:</span> {authNames}
          </p>
        </div>
      </div>
    </div>
  );
};

export default CourseInfo;
