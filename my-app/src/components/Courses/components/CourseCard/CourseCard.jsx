import React from 'react';
import Button from '../../../../common/Button/Button';
import './CourseCard.css';
import pipeDuration from '../../../../helpers/pipeDuration';
import { Link } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import { updateCourseCreator } from '../../../../store/courses/actionCreators';
import trashImg from '../../../../img/trash-24.png';
import pencilImg from '../../../../img/pencil-30.png';
import { deleteCourseDB } from '../../../../store/courses/thunk';

function CourseCard(props) {
  const duration = pipeDuration(props.duration);
  const authNames = props.authors.join(', ');
  const shortDescription = props.description.substring(0, 245);

  const dispatch = useDispatch();

  const role = localStorage.getItem('role');
  const token = useSelector((state) => state.user.token);

  const onDelete = (course) => {
    dispatch(deleteCourseDB(course, token)); ////////////// dispatch
  };

  const onUpdate = (course) => {
    dispatch(updateCourseCreator(course));
  };

  return (
    <div className='courseCard'>
      <div className='courseCard__1'>
        <h2>{props.title}</h2>
        <p>{shortDescription}</p>
      </div>
      <div className='courseCard__2'>
        <p>
          <span>Authors:</span> {authNames}
        </p>
        <p>
          <span>Duration:</span> {duration}
        </p>
        <p>
          <span>Created:</span> {props.creationDate}
        </p>
        <div className='courseCard__btns'>
          <Link to={`/courses/${props.id}`}>
            <Button className='button' buttonText='Show course' />
          </Link>
          {role === 'admin' && (
            <React.Fragment>
              <Link to={`/courses/update/${props.id}`}>
                <Button
                  className='button2'
                  style={{
                    backgroundImage: `url(${pencilImg})`,
                  }}
                  handleClick={() => onUpdate(props)}
                />
              </Link>
              <Button
                className='button2'
                style={{
                  backgroundImage: `url(${trashImg})`,
                }}
                handleClick={() => onDelete(props)}
              />
            </React.Fragment>
          )}
        </div>
      </div>
    </div>
  );
}

export default CourseCard;
