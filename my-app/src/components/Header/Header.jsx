import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import './Header.css';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

function Header(props) {
  const userName = useSelector((state) => state.user.name);
  return (
    <div className='header'>
      <Logo />
      {props.token && (
        <div className='headerIn'>
          <h4>{userName}</h4>
          <Link to='/login'>
            <Button
              className='button'
              buttonText='Logout'
              handleClick={props.onLogout}
            />
          </Link>
        </div>
      )}
    </div>
  );
}

export default Header;
