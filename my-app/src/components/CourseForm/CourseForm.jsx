import React, { useState } from 'react';
import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import './CreateCourse.css';
import pipeDuration from '../../helpers/pipeDuration';
import Authors from '../Authors/Authors';
import DeleteAuthors from '../Authors/DeleteAuthors';
import { useNavigate, useParams } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';
import { saveNewAuthorDB } from '../../store/authors/thunk';
import { updateCourseDB } from '../../store/courses/thunk';
import { useEffect } from 'react';

function CreateCourse(props) {
  const { courseId } = useParams();
  const courseUpd = useSelector((state) =>
    state.courses.find((item) => item.id === courseId)
  );
  const authorsData = useSelector((state) => state.authors);
  console.log(authorsData);
  const token = useSelector((state) => state.user.token);

  const [inputTitle, setTitle] = useState(courseUpd?.title || '');
  const [inputDescription, setDescription] = useState(
    courseUpd?.description || ''
  );
  const [inputAuthor, setAuthor] = useState('');
  const [inputDuration, setDuration] = useState(courseUpd?.duration || '');
  const [delAuthors, setDelAuthors] = useState([]);

  const [authorsList, setAuthorsList] = useState(authorsData);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const titleHandler = (event) => {
    setTitle(event.target.value);
  };

  const descriptionHandler = (event) => {
    setDescription(event.target.value);
  };

  const authorHandler = (event) => {
    setAuthor(event.target.value);
  };

  const durationHandler = (event) => {
    setDuration(event.target.value);
  };

  const authorSubmitHandler = (event) => {
    event.preventDefault();
    const authorDataOne = {
      name: String(inputAuthor),
    };
    if (authorDataOne.name.trim().length === 0) {
      alert('Please fill in author name.');
      return;
    }
    dispatch(saveNewAuthorDB(authorDataOne, token));
    setAuthor('');
  };

  useEffect(() => {
    setAuthorsList(authorsData);
  }, [authorsData]);

  const addOneAuthor = (id) => {
    const newAuthorList = authorsList.filter((item) => item.id !== id);
    setAuthorsList(newAuthorList); // Delete from 'Authors'
    const authorDel = authorsList.filter((item) => item.id === id);
    delAuthors.unshift(...authorDel);
    setDelAuthors(delAuthors); // Add to 'Course Authors'
  };

  const deleteOneAuthor = (id) => {
    setDelAuthors((prevState) => {
      const updateDelAuthors = prevState.filter((item) => item.id !== id);
      return updateDelAuthors;
    }); // Delete from 'Course Authors'
    const authorReturn = delAuthors.filter((item) => item.id === id);
    authorsList.push(...authorReturn);
    setAuthorsList(authorsList); // Add to 'Authors'
  };

  const submitHandler = (event) => {
    event.preventDefault();
    const courseData = {
      title: inputTitle,
      description: inputDescription,
      creationDate: new Date().toLocaleDateString('en-GB'),
      duration: Number(inputDuration),
      authors: delAuthors.map((author) => author.id),
    };
    if (
      courseData.title.trim().length === 0 ||
      courseData.description.trim().length === 0 ||
      // !courseData.duration.value ||
      courseData.authors.length === 0
    ) {
      alert('Please fill in all fields.');
      return;
    }
    props.onSaveCourse(courseData);
    setTitle('');
    setDescription('');
    setDuration('');
    navigate('/courses');
  };

  const updateHandler = (event) => {
    event.preventDefault();
    dispatch(
      updateCourseDB(
        {
          ...courseUpd,
          title: inputTitle,
          description: inputDescription,
          duration: Number(inputDuration),
          authors: delAuthors.map((author) => author.id),
        },
        token
      )
    ); ///////////// dispatch
    navigate('/courses');
  };

  // CreateCourse.propTypes = {
  //   inputTitle: PropTypes.string.isRequired,
  // };

  return (
    <div>
      {courseId ? (
        <form className='createCourse' onSubmit={updateHandler}>
          <div className='createCourse__bar'>
            <Input
              labelText='Title'
              value={inputTitle}
              handleChange={titleHandler}
            />
            <Button
              className='button'
              type='submit'
              buttonText='Update course'
            />
          </div>
          <div className='createCourse__descr'>
            <label>Description</label>
            <textarea
              rows={6}
              value={inputDescription}
              onChange={descriptionHandler}
            />
          </div>
          <div className='createCourse__main'>
            <div className='createCourse__main1'>
              <h3>Add author</h3>
              <Input
                placeholderText='Enter author name...'
                labelText='Author name'
                value={inputAuthor}
                handleChange={authorHandler}
              />
              <br></br>
              <Button
                className='button'
                buttonText='Create author'
                handleClick={authorSubmitHandler}
              />
              <br></br>
              <br></br>
              <h3>Duration</h3>
              <Input
                labelText='Duration'
                value={inputDuration}
                handleChange={durationHandler}
              />
              <br></br>
              <div>
                <p>
                  Duration: <span> {pipeDuration(inputDuration)} </span>
                </p>
              </div>
            </div>
            <div className='createCourse__main2'>
              <h3>Authors</h3>
              {authorsList.map((author) => (
                <Authors
                  addAuthor={() => addOneAuthor(author.id)} //inline handler
                  key={author.id}
                  name={author.name}
                />
              ))}
              <h3>Course authors</h3>
              {delAuthors.length > 0 ? (
                delAuthors.map((author) => (
                  <DeleteAuthors
                    deleteAuthor={() => deleteOneAuthor(author.id)}
                    key={author.id}
                    name={author.name}
                  />
                ))
              ) : (
                <p>Author list is empty.</p>
              )}
            </div>
          </div>
        </form>
      ) : (
        ///////////////////////////////////////////////////////////////////////////////////////////////
        <form className='createCourse' onSubmit={submitHandler}>
          <div className='createCourse__bar'>
            <Input
              placeholderText='Enter title...'
              labelText='Title'
              value={inputTitle}
              handleChange={titleHandler}
            />
            <Button
              className='button'
              type='submit'
              buttonText='Create course'
            />
          </div>
          <div className='createCourse__descr'>
            <label>Description</label>
            <textarea
              rows={6}
              placeholder='Enter description'
              value={inputDescription}
              onChange={descriptionHandler}
            />
          </div>
          <div className='createCourse__main'>
            <div className='createCourse__main1'>
              <h3>Add author</h3>
              <Input
                placeholderText='Enter author name...'
                labelText='Author name'
                value={inputAuthor}
                handleChange={authorHandler}
              />
              <br></br>
              <Button
                className='button'
                buttonText='Create author'
                handleClick={authorSubmitHandler}
              />
              <br></br>
              <br></br>
              <h3>Duration</h3>
              <Input
                placeholderText='Enter duration in minutes...'
                labelText='Duration'
                value={inputDuration}
                handleChange={durationHandler}
              />
              <br></br>
              <div>
                <p>
                  Duration: <span> {pipeDuration(inputDuration)} </span>
                </p>
              </div>
            </div>
            <div className='createCourse__main2'>
              <h3>Authors</h3>
              {authorsList.map((author) => (
                <Authors
                  addAuthor={() => addOneAuthor(author.id)} //inline handler
                  key={author.id}
                  name={author.name}
                />
              ))}

              <h3>Course authors</h3>
              {delAuthors.length > 0 ? (
                delAuthors.map((author) => (
                  <DeleteAuthors
                    deleteAuthor={() => deleteOneAuthor(author.id)}
                    key={author.id}
                    name={author.name}
                  />
                ))
              ) : (
                <p>Author list is empty.</p>
              )}
            </div>
          </div>
        </form>
      )}
    </div>
  );
}

export default CreateCourse;
