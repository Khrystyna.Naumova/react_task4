import Button from '../../common/Button/Button';

function Authors(props) {
  return (
    <div className='createCourse__authors'>
      <p>{props.name}</p>
      <Button
        className='button'
        type='button'
        buttonText='Add author'
        handleClick={props.addAuthor}
      />
    </div>
  );
}

export default Authors;
