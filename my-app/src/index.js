import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import { BrowserRouter } from 'react-router-dom';

import { store } from './store';
import { Provider } from 'react-redux';
import { getCourses } from './services';
import { getAuthors } from './services';

store.dispatch(getCourses());
store.dispatch(getAuthors());

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);
